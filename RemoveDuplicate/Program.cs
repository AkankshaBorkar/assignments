﻿
using System;
using System.Linq;
class RemoveDuplicate
{
    public static void Main()
    {
        int[] data = { 2, 3, 4, 2, 3, 6, 4, 5 };
        Console.WriteLine("Array before removing duplicate values: ");
        Array.ForEach(data, i => Console.WriteLine(i));
        int[] unique = data.Distinct().ToArray();
        Console.WriteLine("Array after removing duplicate values: ");
        Array.ForEach(unique, j => Console.WriteLine(j));
        Console.ReadLine();
    }
}
