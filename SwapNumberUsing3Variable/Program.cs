﻿using System;

namespace SwapNumberUsing3Variable
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int firstnumber = 5, secondnumber = 3, temp;
            temp = firstnumber;
            firstnumber = secondnumber;
            secondnumber = temp;

            Console.WriteLine("Values after swapping are:");
            Console.WriteLine("firstnumber=" + firstnumber);
            Console.WriteLine("secondnumber=" + secondnumber);
            Console.ReadLine();
        }
    }
}
