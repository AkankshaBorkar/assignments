﻿using System;

namespace Multiplication
{
    public class Multiply
    {
        public void Multiplication(int[] ArrayElement)
        {
            int size = ArrayElement.Length;
            int[] temp = new int[size];
            int multiple = 1;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (ArrayElement[j] != ArrayElement[i])
                    {
                        multiple = multiple * ArrayElement[j];
                    }


                }
                temp[i] = multiple;
                multiple = 1;
            }
            Console.WriteLine("The Result is: ");
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(temp[i]);
            }
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("Enter size of array");
            int length = Convert.ToInt32(Console.ReadLine());

            int[] ArrayValues = new int[length];
            Console.WriteLine("Enter the Elements");
            for (int i = 0; i < length; i++)
            {
                ArrayValues[i] = Convert.ToInt32(Console.ReadLine());
            }

            Multiply obj = new Multiply();

            obj.Multiplication(ArrayValues);

        }
    }
}

