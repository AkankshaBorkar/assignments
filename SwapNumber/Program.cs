﻿using System;

namespace SwapNumber
{
    public class Program
    {
        static void Main(string[] args)
        {
            int firstnumber = 5, secondnumber = 10;
            Console.WriteLine("Before swap firstnumber= " + firstnumber + " secondnumber= " + secondnumber);
            firstnumber = firstnumber + secondnumber;      
            secondnumber = firstnumber - secondnumber;       
            firstnumber = firstnumber - secondnumber;   
            Console.Write("After swap firstnumber= " + firstnumber + " secondnumber= " + secondnumber);
            Console.ReadLine();
        }
    }
}
