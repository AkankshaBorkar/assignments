﻿using System;

namespace SecondMaxNumber
{
    public class Program
    {
        public static void PrintSecondMaxNumber(int[] array, int array_size)
        {
            int i, firstnumber, secondnumber;
            if (array_size < 2)
            {
                Console.WriteLine(" Invalid Input ");
                return;
            }
            firstnumber = secondnumber = int.MinValue;
            for (i = 0; i < array_size; i++)
            {
                if (array[i] > firstnumber)
                {
                    secondnumber = firstnumber;
                    firstnumber = array[i];
                }
                else if (array[i] > secondnumber && array[i] != firstnumber)
                    secondnumber = array[i];
            }
            if (secondnumber == int.MinValue)
                Console.Write("No second Maximum" + " Number\n");
            else
                Console.Write("Second Maximum Number" + " is " + secondnumber);
        }
        public static void Main(String[] args)
        {
            int[] array = { 5,8,9,6,12,8 };
            int number = array.Length;
            PrintSecondMaxNumber(array, number);
            Console.ReadLine();
        }
    }
}