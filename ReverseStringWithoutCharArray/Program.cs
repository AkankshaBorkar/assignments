﻿using System;

namespace ReverseStringWithoutCharArray
{
    public class Program
    {
        public static string ReverseString(string word)
        {
            string result = "";
            int length = word.Length - 1;
            while (length >= 0)
            {
                result = result + word[length];
                length--;

            }
            return result;
        }
        public static void Main(string[] arg)
        {
            string name = "Techspian";
            string result = ReverseString(name);
            Console.WriteLine(name);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
